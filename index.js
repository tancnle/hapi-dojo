'use strict';

const Hapi = require('hapi');
const Boom = require('boom');
const Path = require('path');

const server = new Hapi.Server();
server.connection({
  host: 'localhost',
  port: 8000
});

let goodOptions = {
  reporters: [{
    reporter: require('good-console'),
    events: { log: ['*'], response: '*'}
  }]
};

function handler(request, reply) {
  reply(request.params);
}

server.register(
  [
    {
      register: require('good'),
      options: goodOptions
    },
    {
      register: require('inert')
    }
  ],
  err => {
    server.route({
      method: 'GET',
      path: '/',
      handler: (request, reply) => {
        reply({hello: 'world'})
          .code(418)
          .header('hello', 'world')
          .state('hello', 'world');
      }
    });

    server.route({
      method: 'GET',
      path: '/users/{userId?}',
      handler: handler
    });

    server.route({
      method: 'GET',
      path: '/files/{file*}',
      handler: (request, reply) => {
        reply(Boom.notFound());
      }
    });

    server.route({
      method: 'GET',
      path: '/cogent.png',
      handler: {
        file: Path.join(__dirname, 'public/images/cogent.png')
      }
    });

    server.start(() => console.log(`Started at: ${server.info.uri}`));
  }
);
